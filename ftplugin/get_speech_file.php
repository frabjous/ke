<?php

session_start();

require_once '../default_authentication.php';
require $_SERVER["DOCUMENT_ROOT"] . '/kcklib/pipe.php';
require_once 'libtts.php';

$spid = $_GET["spid"];
$fn = $_GET["fn"];
$ftype = $_GET["ftype"] ?? 'none'; // currently unused as all are the same

$my_dir = sys_get_temp_dir() . '/' . $spid;
mkdir($my_dir);
chdir($my_dir);

if (!file_exists($fn)) {
    echo "ERROR. File must be saved first.";
    exit;
}

$pandoc_result = pipe_to_command('pandoc -s -t plain  "' . $fn . '"', '');

if ($pandoc_result->returnvalue != 0) {
    echo("Pandoc did not work.");
    exit;
}

echo clean_text_for_tts($pandoc_result->stdout);
exit;
