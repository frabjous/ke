function wrapInMath() {
    var s = KE.myCM.getSelection();
    var nws = '<?php math(\'' + s + '\'); ?>';
    KE.myCM.replaceSelection(nws, "around");
}

function wrapInItalics() {
    var s = KE.myCM.getSelection();
    var nws = '<?php italicize(\'' + s + '\'); ?>';
    KE.myCM.replaceSelection(nws, "around");
}

addToOnload( function() {

    // new keymaps
    KE.myCM.addKeyMap({
        "Alt-M" : function() { wrapInMath(); },
        "Alt-I" : function() { wrapInItalics(); }
    });

    //template button
    KE.templateButton = newKEButton('code.svg', 'insert template', function() {
        KE.pipeThrough('cat ftplugin/php/phphdr.php ftplugin/html/template.html');
    });

});

