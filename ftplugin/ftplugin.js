// general commands for all ftplugins

function addToOnload(f = function() {}) {
    window.oldOnload = window.onload;
    window.onload = function() {
        window.oldOnload();
        f();
    }
}

function insertHere(s) {
    KE.myCM.replaceRange(s, KE.myCM.getCursor());
}

function newKEButton(icon, tiptitle, callback) {
    var b = creAdd("div", document.getElementById("buttonbar"));
    b.title = tiptitle;
    b.innerHTML = '<img src="/icons/mono/' + icon + '" alt="' + tiptitle + '" />';
    b.onclick = callback; 
    return b;
}

function randomString(len) {
    var charSet ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}

function speakIt(ftype) {
    KE.speakItFType = ftype;
    if (KE.myCM.isClean(KE.currentGeneration)) {
        speakItGo(ftype);
    } else {
        kckYesNoBox(
            "Save changes first?",
            function() {
                KE.save();
                setTimeout( function() { speakItGo(KE.speakItFType); }, 1000);
            },
            function() {
                speakItGo(KE.speakItFType);
            }
        );
    }
}

function speakItGo(ftype) {
    if (KE.speakItButton) {
        makeButtonProcessing(KE.speakItButton);
    }
    KE.speakDiv = creAdd("div", document.body);
    KE.speakDiv.style.display = "inline-block";
    KE.speakDiv.style.position = "fixed";
    KE.speakDiv.style.right = "30px";
    KE.speakDiv.style.bottom = "0";
    KE.speakDiv.style.backgroundColor = "black";
    KE.speakDiv.style.color = "gray";
    KE.speakDiv.style.zIndex = "500";
    KE.speakDiv.style.fontFamily = "sans-serif";
    KE.speakDiv.style.verticalAlign = "top";
    KE.speakDiv.spid = randomString(10);
    KE.innerSDiv = creAdd("div", KE.speakDiv);
    KE.innerSDiv.innerHTML = '<img src="/icons/wait.gif" alt="loading..." />';
    var spkrScript =  'ftplugin/get_speech_file.php';
    var getstr = 'spid=' + encodeURIComponent(KE.speakDiv.spid) + '&fn=' + encodeURIComponent(KE.filename) + '&ftype=' + encodeURIComponent(ftype);
    AJAXGetRequest(spkrScript, getstr, function (text) {
        loadSpeakItText(text); 
    });
}

function loadSpeakItText(text) {
    KE.innerSDiv.innerHTML = '';
    KE.speakArray = text.split(/[\.\!]/).filter(function(s) { return (/[a-zA-Z]/.test(s)); });
    KE.speakAudio = creAdd("audio", KE.innerSDiv);
    KE.speakAudio.controls = "controls";
    KE.speakAudio.style.float = "left";
    KE.speakCtrF = creAdd("input", KE.innerSDiv);
    KE.speakCtrF.type = "number";
    KE.speakCtrF.style.width='3em';
    KE.speakCtrF.style.marginLeft = "1em";
    KE.speakCtrF.value = 0;
    KE.speakCtrF.style.textAlign="right";
    KE.speakCtrTot = creAdd("span", KE.innerSDiv);
    KE.speakCtrTot.innerHTML = "&nbsp;of " + KE.speakArray.length + ' parts&nbsp;';
    playNextPart();
}

function playNextPart() {
    KE.speakAudio.pause();
    if (KE.speakCtrF.value < KE.speakArray.length) {
        KE.speakCtrF.value++;
        var getstr = 'spid=' + encodeURIComponent(KE.speakDiv.spid) + '&part=' + encodeURIComponent(KE.speakCtrF.value - 1) + '&here=' + encodeURIComponent(KE.speakArray[KE.speakCtrF.value - 1]);
        if (KE.speakCtrF.value < KE.speakArray.length) {
            getstr+= '&there=' + encodeURIComponent(KE.speakArray[KE.speakCtrF.value]);
        }
        KE.speakAudio.onended = function() {  playNextPart(); };
        KE.speakAudio.src = 'ftplugin/get_audio_part.php?' + getstr;
        KE.speakAudio.oncanplay = function() { makeButtonNormal(KE.speakItButton); KE.speakAudio.play(); };
    }
}

function clearSpeakIt() {
    if (KE.speakAudio) {
        KE.speakAudio.pause();
    }
    if (KE.speakItButton) {
        makeButtonProcessing(KE.speakItButton);
    }
    if (KE.speakDiv) {
        var getstr = 'spid=' + encodeURIComponent(KE.speakDiv.spid);
        AJAXGetRequest('ftplugin/clear_speech_dir.php', getstr, function(text) {
            if (text.trim() == "done") {
                makeButtonInactive(KE.speakItButton);
            }
        });
        document.body.removeChild(KE.speakDiv);
        KE.speakDiv = {};
    }
    makeButtonInactive(KE.speakItButton);
}

/////// AutoProcess Shared stuff


function keToggleAutoProcess() {
    if (KE.autoProcess) {
        // turn off
        if (KE.hasOwnProperty("autoprocessButton")) {
            makeButtonInactive(KE.autoprocessButton);
        }
        KE.autoProcess = false;
        KE.onchangeExtras = function() {};
    } else {
        // turn on
        if (KE.hasOwnProperty("autoprocessButton")) {
            makeButtonNormal(KE.autoprocessButton);
        }
        if (!(KE.prevIsOpen)) {
            keTogglePreview();
        }
        KE.autoProcess = true;
        keStartAutoProcess();
    }
}

function keStartAutoProcess() {
    KE.onchangeExtras = function() {
        keAutoProcessStartTO();
    }
    if (!(KE.myCM.isClean(KE.currentGeneration))) {
        keAutoProcessGo();
    }
}

function keTogglePreview() {
    if (!(KE.hasOwnProperty("previewButton"))) {
        return;
    }
    if (KE.previewButton.classList.contains("inactive")) {
        makeButtonNormal(KE.previewButton);
        showResults();
        setTimeout( function() { keAutoProcessGo() }, 1000);
    } else {
        makeButtonInactive(KE.previewButton);
        hideResults();
    }
}

function kePreviewRefresh() {
    if (!(KE.prevIsOpen)) {
        keTogglePreview();
        return;
    }
    if (typeof KE.refreshPreview == "function") {
        KE.refreshPreview();
    } else {
        KE.results.location.href = KE.resultsLoc;
        KE.results.location.reload(true);
    }    
}

function keAutoProcessStartTO() {
    clearTimeout(KE.autoProcessTO);
    KE.autoProcessTO = setTimeout(
        function() { keAutoProcessGo(); }
        , KE.autoProcessDelay);
}

function keAutoProcessGo() {
    if (KE.hasOwnProperty("goButton")) {
        makeButtonProcessing(KE.goButton);
    }
    KE.save(KE.filename, KE.processOption);
}
