
addToOnload( function() {

    // initial variables
    KE.handlerName = 'html';
    KE.processOption = 'html'
    KE.resultsLoc = '/' + KE.filename.split('/http/').slice(-1)[0];

    KE.postSaveHandlers.html = function(resp) {
        if (resp.processSuccess) {
            makeButtonNormal(KE.goButton);
            kePreviewRefresh();
        } else {
            makeButtonError(KE.goButton);
        }
    }

    //template button
    KE.templateButton = newKEButton('code.svg', 'insert template', function() {
        KE.pipeThrough('cat ftplugin/html/template.html');
    });

    
    // only provide live preview buttons if on webserver root
    if (fileIsOnDocRoot) {
    
        // preview button
        KE.goButton = newKEButton('play.svg', 'update preview', function() {
            if (!(KE.goButton.classList.contains("processing"))) {
                keAutoProcessGo(); 
            }
        });

        // results button
        KE.previewButton = newKEButton('public.svg', 'view result', function() {
            keTogglePreview();
        });
        makeButtonInactive(KE.previewButton);


        // autopreview button
        KE.autoprocessButton = newKEButton('playcircle.svg', 'autopreview', function() {
            keToggleAutoProcess();
        });
        makeButtonInactive(KE.autoprocessButton);

    }

    // speakit button
    KE.speakItButton = newKEButton('volup.svg', 'speak outloud', function() {
        if (KE.speakItButton.classList.contains("inactive")) {
            speakIt("html");
        } else {
            clearSpeakIt();
        }
    });
    makeButtonInactive(KE.speakItButton);

});

