<?php
session_start();

require_once '../default_authentication.php';

if (!isset($_GET["spid"])) {
    exit("spid not set");
}

$spid = $_GET["spid"];

$my_dir = sys_get_temp_dir() . '/' . $spid;

if (is_dir($my_dir)) {
    chdir($my_dir);
    $files = scandir($my_dir);
    foreach ($files as $file) {
        if (($file=='.') || ($file=='..')) {
            continue;
        }
        unlink($file);
    }
    chdir(sys_get_temp_dir());
    rmdir($my_dir); 
}  
echo "done";
exit;
