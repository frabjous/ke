<?php
set_time_limit(0);
session_start();
require $_SERVER["DOCUMENT_ROOT"] . '/kcklib/stream.php';

require_once '../default_authentication.php';

$spid = $_GET["spid"];
$part = $_GET["part"];
$here = $_GET["here"];
$there = $_GET["there"] ?? '';
$here = str_replace("'", "", $here);
$here = str_replace('"', "", $here);
$there = str_replace("'", "", $there);
$there = str_replace('"', "", $there);

$my_dir = sys_get_temp_dir() . '/' . $spid;
if (file_exists($my_dir)) {
   chdir($my_dir);
} else {
   exit;
}

// start next process
$pid = 0;
if ($there != '') {
   $that_mp3 = 'part' . (intval($part)+1) . '.mp3';
   $that_wav = 'part' . (intval($part)+1) . '.wav';
   exec('(echo "' . $there . '" | flite_cmu_us_slt - "' . $that_wav . '" && lame --quiet -f "' . $that_wav .'"  "' . $that_mp3 . '") >/dev/null  2>&1 & echo $!', $o, $e);
   
   file_put_contents('part' . (intval($part)+1) . '.pid', $o);
}

$this_wav = 'part' . $part . '.wav';
$this_mp3 = 'part' . $part . '.mp3';
$this_pid_file = 'part' . $part . '.pid';

// check if this one going
if (file_exists($this_pid_file)) {
   $this_pid = trim(file_get_contents($this_pid_file));
   for ($i=0; $i<32; $i++) {
      if (trim(shell_exec('ps hp ' . $this_pid)) == '') {
         break;
      }
      usleep(250000);
   }
}

if (file_exists($this_mp3)) {
   stream($this_mp3);
   exit(0);
}

exec('echo "' . $here . '" | flite_cmu_us_slt - "' . $this_wav . '" && lame --quiet -f "' . $this_wav .'"  "' . $this_mp3 . '"', $o, $e);
   
if (($e != 0) || (!(file_exists($this_mp3)))) {
    exit;
}

stream($this_mp3);
exit(0);
