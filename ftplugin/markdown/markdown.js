function mdPdfRefresh() {
    KE.results.postMessage({ 
        refresh : true
    }, '*');
}

addToOnload( function() {

    // initial variables
    KE.handlerName = 'markdown';
    KE.processOption = 'markdown';
    KE.resultsLoc = 'ftplugin/markdown/preview.html';
    KE.refreshPreview = function() { mdPdfRefresh(); }

    KE.postSaveHandlers.markdown = function(resp) {
        if (resp.processSuccess) {
            makeButtonNormal(KE.goButton);
            kePreviewRefresh();
        } else {
            makeButtonError(KE.goButton);
        }
    }

    // preview button
    KE.goButton = newKEButton('play.svg', 'go', function() {
        if (!(KE.goButton.classList.contains("processing"))) {
            keAutoProcessGo(); 
        }
    });

    // results button
    KE.previewButton = newKEButton('public.svg', 'view result', function() {
        keTogglePreview();
    });
    makeButtonInactive(KE.previewButton);

    // autopreview button
    KE.autoprocessButton = newKEButton('playcircle.svg', 'autopreview', function() {
        keToggleAutoProcess();
    });
    makeButtonInactive(KE.autoprocessButton);

    // speakit button
    KE.speakItButton = newKEButton('volup.svg', 'speak outloud', function() {
        if (KE.speakItButton.classList.contains("inactive")) {
            speakIt("markdown");
        } else {
            clearSpeakIt();
        }
    });
    makeButtonInactive(KE.speakItButton);

});

