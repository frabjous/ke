<?php

// note you can add a css file and resource path 
// to the pandoc preview command by creating a 
// file in this folder pandoc-preview-options.json
// with this format:
/*

{
    "pandoc_css": "/home/path/to/myfile.css",
    "pandoc_resource_path": "/home/someone/pandoc/resources/"
}

*/

function pipe_to_command($cmd, $pipe_text) {

    $rv = new StdClass();

    $descriptorspec = array(
        0 => array("pipe", "r"), // stdin
        1 => array("pipe", "w"), // stdout 
        2 => array("pipe", "w")  // stderr
    );

    $process = proc_open($cmd, $descriptorspec, $pipes);

    if (is_resource($process)) {
        fwrite($pipes[0], $pipe_text);
        fclose($pipes[0]);

        $rv->stdout = stream_get_contents($pipes[1]);
        $rv->stderr = stream_get_contents($pipes[2]);
        fclose($pipes[1]);
        fclose($pipes[2]);

        $rv->returnvalue = proc_close($process);

    }

    return $rv;

}

if (file_exists($olddir . '/ftplugin/markdown/pandoc-preview-options.json')) {
    $pandoc_preview_options = json_decode(file_get_contents($olddir . '/ftplugin/markdown/pandoc-preview-options.json'));
} else {
    $pandoc_preview_options = new StdClass();
}

$pandoc_command = 'pandoc -s --self-contained -t html5+smart ';
if (isset($pandoc_preview_options->pandoc_css)) {
    $pandoc_command .= '--css "' . $pandoc_preview_options->pandoc_css . '" ';
}
if (isset($pandoc_preview_options->pandoc_resource_path)) {
    $pandoc_command .= '--resource-path "' . $pandoc_preview_options->pandoc_resource_path . '" ';
}
$pandoc_command .= '-o "' . sys_get_temp_dir() . '/ke-pandoc-preview.html"';

$piperesult = pipe_to_command($pandoc_command, $sent_contents);
error_log($pandoc_command);
error_log($piperesult->stderr);
error_log($piperesult->stdout);
error_log($piperesult->returnvalue);

$ret_val->processSuccess = ($piperesult->returnvalue == 0);
