<?php
session_start();

require_once '../../default_authentication.php';

$ret_val = new stdClass();

function rage_quit($errcode) {
    global $ret_val;
    $ret_val->exitcode = $errcode;
    echo(json_encode($ret_val, JSON_PRETTY_PRINT));
    exit($errcode);
}

$outputfile = urldecode($_GET["outputfile"]);
$pagenum = urldecode($_GET["pagenum"]);
$x = intval(urldecode($_GET["x"]));
$y = intval(urldecode($_GET["y"]));

chdir(dirname($outputfile));
$obn = basename($outputfile);
exec('synctex edit -o ' . $pagenum . ":" . $x . ":" . $y . ":" . $obn . " -x 'echo kesyncoutput:%{input}:%{line}:%{column}'", $output, $rv);

if ($rv == 0) {
   foreach ($output as $line) {
      if (preg_match('/^kesyncoutput/', $line)) {
         $syncoutput = explode(':',$line);
         break;
      }
   }
}

if ((!(isset($syncoutput))) or (count($syncoutput) < 4)) {
   rage_quit(1);
}

$ret_val->fileName = realpath($syncoutput[1]);
$ret_val->line = $syncoutput[2];
$ret_val->ch = $syncoutput[3];
echo(json_encode($ret_val, JSON_PRETTY_PRINT));
exit(0);
?>
