<?php

$filename = $filename_to_save;

$five_lines = explode(PHP_EOL, $sent_contents, 6);

$procoptions = array(new stdClass());

for ($i=0; (($i<5) and ($i<count($five_lines))) ; $i++) {
   $exp = explode("keprocessOptions =", $five_lines[$i]);
   if (count($exp) > 1) {
      $procoptions = json_decode(trim($exp[1])) ?? $procoptions;
   }
}

function process_rage_quit($e) {
   global $ret_val;
   $ret_val->processSuccess = false;
   $ret_val->processExitCode = $e;
   echo(json_encode($ret_val, JSON_PRETTY_PRINT));
   exit(0);
}

foreach ($procoptions as $this_process) {

   $texcommand = $this_process->command ?? 'pdflatex';
   
   if (($texcommand == 'pdflatex') or ($texcommand == 'latex') or ($texcommand == 'lualatex') or ($texcommand == 'xelatex')) {
   
      $target = $this_process->target ?? $filename;
      chdir(dirname($filename));
      if (!(file_exists($target))) {       
         process_rage_quit(1);
      }
          
      $targetdir = dirname(realpath($target));
      $targetbn = basename($target);

      chdir($targetdir);
      $options = $this_process->options ?? '-interaction=nonstopmode -synctex=1';

      exec('timeout 30 ' . $texcommand . ' ' . $options . ' ' . $targetbn . ' 2>&1', $output, $exitcode);

      $pi = pathinfo($targetbn);
      $outputfilename = $pi['filename'] . '.pdf';
      if (file_exists($outputfilename)) {
         $ret_val->outputfilename = realpath($outputfilename);
      }      
      if ($exitcode != 0) {
         process_rage_quit($exitcode);
      } 
   } elseif (($texcommand == 'dvips') or ($texcommand == 'dvipdf') or ($texcommand == 'dvipdfm') or ($texcommand == 'dvipdfmx')) {
      $pi = pathinfo($filename);
      $target = $this_process->target ?? $pi['dirname'] . '/' . $pi['filename'] . '.dvi';
      chdir(dirname($filename));
      if (!(file_exists($target))) {       
         process_rage_quit(1);
      }
          
      $targetdir = dirname(realpath($target));
      $targetbn = basename($target);

      chdir($targetdir);

      $options = $this_process->options ?? '';

      exec('timeout 30 ' . $texcommand . ' ' . $options . ' ' . $targetbn, $output, $exitcode);

      $pi = pathinfo($targetbn);
      $outputfilename = $pi['filename'] . '.pdf';
      if (file_exists($outputfilename)) {
         $ret_val->outputfilename = realpath($outputfilename);
      }      
      if ($exitcode != 0) {
         process_rage_quit($exitcode);
      } 
   } elseif ($texcommand == 'ps2pdf') {
      $pi = pathinfo($filename);
      $target = $this_process->target ?? $pi['dirname'] . '/' . $pi['filename'] . '.ps';
      chdir(dirname($filename));
      if (!(file_exists($target))) {       
         process_rage_quit(1);
      }
          
      $targetdir = dirname(realpath($target));
      $targetbn = basename($target);

      chdir($targetdir);

      $options = $this_process->options ?? '';

      exec('timeout 30 ' . $texcommand . ' ' . $options . ' ' . $targetbn, $output, $exitcode);

      $pi = pathinfo($targetbn);
      $outputfilename = $pi['filename'] . '.pdf';
      if (file_exists($outputfilename)) {
         $ret_val->outputfilename = realpath($outputfilename);
      }      
      if ($exitcode != 0) {
         process_rage_quit($exitcode);
      }      
   } elseif (($texcommand == 'bibtex') or ($texcommand == 'biber')) {
      $pi = pathinfo($filename);
      if ($texcommand == 'biber') {
         $cf_ext = '.bcf';
      } else {
         $cf_ext = '.aux';
      }
      $target = $this_process->target ?? $pi['dirname'] . '/' . $pi['filename'] . $cf_ext;
      chdir(dirname($filename));
      if (!(file_exists($target))) {       
         process_rage_quit(1);
      }
          
      $targetdir = dirname(realpath($target));
      $targetbn = basename($target);

      chdir($targetdir);

      $options = $this_process->options ?? '';

      exec('timeout 30 ' . $texcommand . ' ' . $options . ' ' . $targetbn, $output, $exitcode);

      if ($exitcode != 0) {
         process_rage_quit($exitcode);
      }
   }
}   

$ret_val->processSuccess = true;
if (isset($ret_val->outputfilename)) {
   $ret_val->numPages = trim(shell_exec('mutool info ' . $ret_val->outputfilename . '  | grep \'^Pages:\' | sed \'s/[^0-9]//g\''));
}
