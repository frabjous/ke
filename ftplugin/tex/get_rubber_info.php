<?php

session_start();

require_once '../../default_authentication.php';

if (!(isset($_GET["logfile"]))) {
    echo('No log file given.');
    exit(0);
}

$logfile = urldecode($_GET["logfile"]);

chdir(dirname($logfile));
$ri = shell_exec('rubber-info ' . $logfile . ' 2>&1');

if ($ri != '') {
    echo $ri; 
} else {
    echo 'No errors reported.';
}
