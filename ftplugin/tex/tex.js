
function texGetLogFilename() {
    var fname = KE.filename;
    var ffl = KE.myCM.getRange({ line: 0, ch: 0 }, { line: 5, ch: 0 });
    var ffla = ffl.split("\n");
    for (var i=0; i<ffla.length; i++) {
        if (/keprocessOptions =/.test(ffla[i])) {
            var optstext = ffla[i].replace(/.*processOptions\s*=\s*/, '').trim();
            var o;
            try {
                o = eval(optstext);
            } catch(e) { };
            if ((typeof o == "undefined") || (!(Array.isArray(o)))) {
                continue;
            }
            for (var j=0; j<o.length; j++) {
                if (o[j].hasOwnProperty("target")) {
                    fname = o[j].target;
                    break;
                }
            }
            if (fname != KE.filename) { break; }
        }
    }
    return fname.replace(/\.[^.$]+$/, '') + '.log';
}

function texDblQuotationMark() {
    var crsr = KE.myCM.getCursor();
    if (crsr.ch == 0) {
        insertHere('``');
        return;
    }
    var prevspot = { line: crsr.line, ch: (crsr.ch - 1) };
    var prevchar = KE.myCM.getRange(prevspot, crsr);
    if (prevchar == "\\") {
        insertHere('"');
        return;
    }
    var leftQPrevs = ['(', '{', '[', ' ', '&', '='];
    if (leftQPrevs.includes(prevchar)) {
        insertHere("``");
        return;
    }
    insertHere("''");
}

function texSingQuotationMark() {
    var crsr = KE.myCM.getCursor();
    if (crsr.ch == 0) {
        insertHere('`');
        return;
    }
    var prevspot = { line: crsr.line, ch: (crsr.ch - 1) };
    var prevchar = KE.myCM.getRange(prevspot, crsr);
    var leftQPrevs = ['(', '{', '[', ' ', '&', '='];
    if (leftQPrevs.includes(prevchar)) {
        insertHere("`");
        return;
    }
    insertHere("'");
}

function texPdfRefresh() {
    var gotopage = 0;
    if (KE.hasOwnProperty("gotopage")) {
        gotopage = KE.gotopage;
    }
    KE.results.postMessage({ 
        filename : KE.texPdfViewer.filename,
        numpages: KE.texPdfViewer.numPages,
        pagenum : gotopage
    }, '*');
    KE.gotopage = 0;
}


function texFinishSynctexForward(sdata) {
    KE.gotopage = sdata.pageNum;
    kePreviewRefresh();
    makeButtonNormal(KE.synctexForwardButton);
    KE.myCM.focus();
}

function texStartSynctexForward() {
    if ((KE.filename == '') || (KE.texPdfViewer.filename == '')) {
        kckErrAlert("You must compile at least once first.");
        return;
    }
    makeButtonProcessing(KE.synctexForwardButton);
    var ci = KE.myCM.getCursor();   
    var line = (ci.line + 1);
    var ch = (ci.ch + 1);
    var getstr = 'outputfile=' + encodeURIComponent(KE.texPdfViewer.filename) + '&inputfile=' + encodeURIComponent(KE.filename) + '&line=' + line + '&ch=' + ch;
    AJAXGetRequest('ftplugin/tex/synctex-forward.php', getstr, function(text) {
        texFinishSynctexForward(JSON.parse(text));
    });
}

function texShowErrors() {
    var logf = texGetLogFilename();
    AJAXGetRequest('ftplugin/tex/get_rubber_info.php', 'logfile=' + encodeURIComponent(logf), function(text) {
        KE.myCM.openNotification('<pre>' + text + '</pre>', { duration: 2000 });
        var fls = text.split("\n")[0].split(":");
        if (fls[0] == KE.filename.split('/').pop()) {
            KE.myCM.setCursor({line: (fls[1] - 1), ch: 0 });
        }
        KE.myCM.focus();
    });
}

function emphWrap() {
    var newText = '\\emph{' + KE.myCM.getSelection() + '}';
    KE.myCM.replaceSelection(newText, "around");
}

//////////////////////////////////// after loading
addToOnload( function() {

    // new keymaps
    KE.myCM.addKeyMap({
        "'\"'" : function() { texDblQuotationMark(); },
        "'\''" : function() { texSingQuotationMark(); },
        "Alt-J" : function() { texStartSynctexForward(); },
        "Alt-E" : function() { emphWrap(); }
    });

    // initial variables
    KE.handlerName = 'tex';
    KE.processOption = 'tex';
    KE.resultsLoc = 'ftplugin/tex/preview.html';
    KE.gotopage = 0;
    KE.texPdfViewer = {};
    KE.texPdfViewer.filename = '';
    KE.texPdfViewer.numPages = 1;
    KE.refreshPreview = function() { texPdfRefresh(); }

    KE.postSaveHandlers.tex = function(resp) {
        if (resp.processSuccess) {
            makeButtonNormal(KE.goButton);
            if (resp.hasOwnProperty("numPages")) {
                KE.texPdfViewer.numPages = resp.numPages;
            }
            if (resp.hasOwnProperty("outputfilename")) {
                KE.texPdfViewer.filename = resp.outputfilename;
                if (KE.prevIsOpen) {
                    kePreviewRefresh();
                }
            }
        } else {
            makeButtonError(KE.goButton);
        }
    }

    //template button
    KE.templateButton = newKEButton('braces.svg', 'insert template', function() {
        KE.pipeThrough('cat ftplugin/tex/template.tex');
    });

    // compile button
    KE.goButton = newKEButton('play.svg', 'compile', function() {
        if (!(KE.goButton.classList.contains("processing"))) {
            keAutoProcessGo(); 
        }
    });

    // results button
    KE.previewButton = newKEButton('pdf.svg', 'view result', function() {
        keTogglePreview();
    });
    makeButtonInactive(KE.previewButton);

    // synctex forward button
    KE.synctexForwardButton = newKEButton('jump.svg', 'synctex forward search', function() {
        texStartSynctexForward();
    });

    // autocompile button
    KE.autoprocessButton = newKEButton('playcircle.svg', 'autocompile', function() {
        keToggleAutoProcess();
    });
    makeButtonInactive(KE.autoprocessButton);

    // showerrors button
    KE.showErrorsButton = newKEButton('alert.svg', 'show errors', function() {
        texShowErrors();
    });

    // speakit button
    KE.speakItButton = newKEButton('volup.svg', 'speak outloud', function() {
        if (KE.speakItButton.classList.contains("inactive")) {
            speakIt("tex");
        } else {
            clearSpeakIt();
        }
    });
    makeButtonInactive(KE.speakItButton);

    // listen for synctex reverse messages
    window.addEventListener("message", receiveMessage, false);
    function receiveMessage(event) {
        var gotoInfo = event.data;
        KE.myCM.focus();
        KE.myCM.setCursor({ line: gotoInfo.line, ch: gotoInfo.ch });     
    }
});

