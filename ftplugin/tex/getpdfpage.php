<?php
session_start();

require_once '../../default_authentication.php';

$doc_root = $_SERVER["DOCUMENT_ROOT"];

if (!isset($_GET["file"])) {
    exit("No file provided.");
}

$file = $_GET["file"];
$page = $_GET["pagenum"] ?? 1;

$temp_png = sys_get_temp_dir() . '/ketexpreview.png';

exec('mutool draw -r 300 -o "' .  $temp_png . '" "' . $file . '" ' . $page, $output, $rv);

if ($rv != 0) {
    exit("mupdf failed");
}

header("Content-Type:image/png");
header("Content-Length:".filesize($temp_png));
header("Cache-Control: no-cache, must-revalidate");
readfile($temp_png);
