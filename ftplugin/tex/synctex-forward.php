<?php
session_start();

require_once '../../default_authentication.php';

$ret_val = new stdClass();

function rage_quit($errcode) {
    global $ret_val;
    $ret_val->exitcode = $errcode;
    echo(json_encode($ret_val, JSON_PRETTY_PRINT));
    exit($errcode);
}

$outputfile = urldecode($_GET["outputfile"]);
$inputfile = urldecode($_GET["inputfile"]);
$line = intval(urldecode($_GET["line"]));
$ch = intval(urldecode($_GET["ch"]));

chdir(dirname($inputfile));
$ibn = basename($inputfile);
$obn = basename($outputfile);
exec('synctex view -i ' . $line . ":" . $ch . ":" . $ibn . " -o " . $obn . " -x 'echo kesyncoutput:%{output}:%{page+1}:%{x}:%{y}:%{width}:%{height}'", $output, $rv);

if ($rv == 0) {
    foreach ($output as $line) {
        if (preg_match('/^kesyncoutput/', $line)) {
            $syncoutput = explode(':',$line);
            break;
        }
    }
}

if ((!(isset($syncoutput))) or (count($syncoutput) < 7)) {
    rage_quit(1);
}

$ret_val->fileName = $syncoutput[1];
$ret_val->pageNum = $syncoutput[2];
$ret_val->x = $syncoutput[3];
$ret_val->y = $syncoutput[4];
$ret_val->w = $syncoutput[5];
$ret_val->h = $syncoutput[6];
echo(json_encode($ret_val, JSON_PRETTY_PRINT));
exit(0);
