<?php 

session_start();
require 'libke.php';
require $_SERVER["DOCUMENT_ROOT"] . '/kcklib/hostinfo.php';

function rage_quit($msg) {
    echo "ERROR: " . $msg;
    exit(1);
}

require_once 'default_authentication.php';

// remember old dir
$olddir = getcwd();

// start in right folder
if ((isset($_SESSION["_ke_workingdir"])) && (is_dir($_SESSION["_ke_workingdir"]))) {
    chdir($_SESSION["_ke_workingdir"]);
}

// theme stuff
$cm_theme = 'rubyblue';

if (isset($_GET["theme"])) {
    $potential_theme = $_GET["theme"];
    if (file_exists('codemirror/theme/' . $potential_theme . '.css')) {
        $cm_theme = $potential_theme;
    }
}

// defaults
$cm_filename = '';

if (isset($_GET["file"])) {
    $cm_filename = $_GET["file"];
    if (file_exists($cm_filename)) {
        $real_filename = realpath($cm_filename);
    } else {
        if (substr($cm_filename, 0, 1) == '/') {
            $real_filename = $cm_filename;
        } else {
            $real_filename = getcwd() . '/' . $cm_filename;
        }
    }
    if ($real_filename != $cm_filename) {
        header('Location: ' . full_path() . '?file=' . urlencode($real_filename) . '&theme=' . $cm_theme);
        exit(0);
    }

    if (!$ke_poweruser) {
        $ok_to_see = false;
        foreach ($_SESSION["_ke_allowed_folders"] as $folder) {
            if (substr($real_filename, 0, strlen($folder)) == $folder) {
                $ok_to_see = true;
                break;
            }
        }

        if (!$ok_to_see) {
            rage_quit("KE user does not have the permissions to save a file in this folder.");
        }
    }

    if (is_dir(dirname($real_filename))) {
        $_SESSION["_ke_workingdir"] = dirname($real_filename);
        chdir(dirname($real_filename));
    }

}


// test if file is in web document root
$on_doc_root = (substr($cm_filename, 0, strlen($_SERVER["DOCUMENT_ROOT"])) == $_SERVER["DOCUMENT_ROOT"]);


$workingdir = getcwd();

$cm_mode_info = get_mode_for_file($cm_filename);

?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- meta stuff -->
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="kevedit.ico" type="image/x-icon" />
        <meta name="robots" content="noindex,nofollow" />
        <title><?php if ($cm_filename != '') { echo basename($cm_filename) . ' | '; } ?>k(ev)e(dit)</title>

        <!-- dependencies -->
        <script type="text/javascript" charset="utf-8" src="/kcklib/ajax.js"></script>    
        <script type="text/javascript" charset="utf-8" src="/kcklib/kckdialog.js"></script>    
        <link rel="stylesheet" type="text/css" href="/kcklib/kckdialog.css" />

        <!-- code mirror -->
        <script src="codemirror/lib/codemirror.js"></script>
        <link rel="stylesheet" href="codemirror/lib/codemirror.css">

        <!-- code mirror addons / -->
        <link rel="stylesheet" type="text/css" href="codemirror/addon/dialog/dialog.css" />
        <script src="codemirror/addon/mode/simple.js"></script> 
        <script src="codemirror/addon/dialog/dialog.js"></script> 
        <script src="codemirror/addon/edit/matchbrackets.js"></script>
        <script src="codemirror/addon/search/searchcursor.js"></script>
        <script src="codemirror/addon/search/search.js"></script>
        <script src="codemirror/addon/search/jump-to-line.js"></script> 

        <!-- code mirror theme -->   
        <link rel="stylesheet" href="codemirror/theme/<?php echo $cm_theme; ?>.css">
        <?php 

        foreach ($cm_mode_info["modefiles"] as $mf) {
            echo '<script src="codemirror/mode/' . $mf . '/' . $mf . '.js"></script>' . PHP_EOL;
        }
        ?>

        <!-- main script and stylesheet -->
        <script type="text/javascript" charset="utf-8" src="ke.js"></script>    
        <link rel="stylesheet" type="text/css" href="ke.css" />

        <!-- page specific inline script -->
        <script type="text/javascript">

            var KE={};
            var fileIsOnDocRoot = <?php if ($on_doc_root) { echo 'true'; } else { echo 'false'; } ?>;

            function fixMCHeight() {
                var h = window.innerHeight;
                var tbh = document.getElementById("toolbarwrapper").offsetHeight;
                var mch = (h - tbh);
                document.getElementById("maincontents").style.height = mch + "px";
                document.getElementById("maincontents").style.top = tbh + "px";
                KE.myCM.refresh();
            }

            // default page onload script
            window.onload = function() {

                // invoke codemirror
                KE = new kevEditor(document.getElementById("keDiv"), "<?php echo $cm_theme; ?>", "<?php echo $cm_mode_info["mimetype"]; ?>", "<?php echo $cm_filename; ?>", <?php if ($cm_mode_info["usewrap"]) { echo 'true';} else {echo 'false';} ?>);

                // tell it where it should save files, maybe
                KE.workingDir = "<?php echo $workingdir; ?>";

                // buttons
                KE.saveButton = document.getElementById("saveb");
                KE.saveButton.onclick = function() {
                    if (!(KE.saveButton.classList.contains("inactive"))) {
                        KE.save();
                    }
                }
                KE.openButton = document.getElementById("openb");
                KE.openButton.onclick = function() {
                    kckFileChoose(function(fn) {KE.open(fn);}, KE.workingDir);
                }
                KE.changeFontButton = document.getElementById("fontb");
                KE.changeFontButton.onclick = function() {
                    KE.changeFont();
                }
                KE.wrapButton = document.getElementById("wrapb");
                <?php if (!($cm_mode_info["usewrap"])) { echo 'makeButtonInactive(KE.wrapButton);'; } ?>
                KE.wrapButton.onclick = function() {
                    KE.toggleWrap();
                    KE.myCM.focus();
                }
                KE.findButton = document.getElementById("findb");
                KE.findButton.onclick = function() {
                    CodeMirror.commands.find(KE.myCM);
                }
                KE.findNextButton = document.getElementById("findnextb");
                KE.findNextButton.onclick = function() {
                    CodeMirror.commands.findNext(KE.myCM);
                }    
                KE.replaceButton = document.getElementById("replaceb");
                KE.replaceButton.onclick = function() {
                    KE.replaceDialog();
                }    
                KE.filterButton = document.getElementById("filterb");
                KE.filterButton.onclick = function() {
                    KE.pipeDialog();
                }    
                KE.readButton = document.getElementById("readb");
                KE.readButton.onclick = function() {
                    KE.readFileDialog();
                }  

                KE.markClean();

                // results area
                KE.results = {}; // document.getElementById("results"); ***
                KE.mc = document.getElementById("maincontents");

                // window size fix
                fixMCHeight();

                // load start file if it exists
                <?php 
                if (file_exists($cm_filename)) { 
                    echo "
    if (KE.filename != '') {
        KE.initialOpen(KE.filename);
    }
	";
                } ?>

                document.getElementById("toolbar").addEventListener("keydown", function(evt) {
                    // CTRL F to find
                    if ((evt.keyCode == 70) && (evt.ctrlKey)) {
                        evt.preventDefault();
                        CodeMirror.commands.find(KE.myCM);
                        document.getElementsByClassName("CodeMirror-search-field")[0].focus();
                    }
                    // CTRL G to findNext
                    if ((evt.keyCode == 71) && (evt.ctrlKey)) {
                        evt.preventDefault();
                        CodeMirror.commands.findNext(KE.myCM);
                    } 
                    // CTRL R to replace
                    if ((evt.keyCode == 82) && (evt.ctrlKey)) {
                        evt.preventDefault();
                        KE.replaceDialog();
                        document.getElementsByClassName("CodeMirror-search-field")[0].focus();
                    }
                    // CTRL S to save
                    if ((evt.keyCode == 83) && (evt.ctrlKey)) {
                        evt.preventDefault();
                        KE.myCM.save();
                    }
                }); 

                // start Autosave Timer
                KE.startAutoSaveTimer();
                KE.myCM.focus();

                setTimeout( function() { fixMCHeight(); }, 100);

            }

            window.onresize = function() {
                fixMCHeight();
            }

            window.onbeforeunload = function() {
                if (!(KE.myCM.isClean(KE.currentGeneration))) {
                    return "If you exit now, you will lose your changes.";
                }
            }

        </script>

        <?php

        // load ftplugin if necessary
        $ftext = $cm_mode_info["extension"];

        if (file_exists($olddir . '/ftplugin/' . $ftext . '/' . $ftext . '.js')) {
            echo '<script type="text/javascript" charset="utf-8" src="ftplugin/ftplugin.js" ></script>' .  PHP_EOL;
            echo '<script type="text/javascript" charset="utf-8" src="ftplugin/' . $ftext . '/' . $ftext . '.js" ></script>' .  PHP_EOL;
        }

        ?>

    </head>
    <body>
        <!-- allcontents contains everything, except what is created by scripts -->
        <div id="allcontents">

            <!-- maincontents contains toolbar, and editor -->
            <div id="maincontents">

                <!-- toolbar -->
                <div id="toolbarwrapper">
                    <div id="toolbar">
                        <div id="buttonbar">
                            <div style="display: none; width: 0; height: 0;"><img src="/icons/wait.gif" /></div><!--
--><div id="saveb" title="save"><img src="/icons/mono/save.svg" alt="save" /></div><!--
--><div id="openb" title="open"><img src="/icons/mono/folder_open.svg" alt="open" /></div><!--
--><div id="fontb" title="choose font" <?php if ($_SERVER['HTTP_HOST'] != 'localhost') { echo 'style="display: none;"'; } ?> ><img src="/icons/mono/font.svg" alt="choose font" /></div><!--
--><div id="wrapb" title="toggle wrap"><img src="/icons/mono/wrap.svg" alt="wrap" /></div><!--
--><div id="findb" title="find"><img src="/icons/mono/find.svg" alt="find" /></div><!--
--><div id="findnextb" title="find next"><img src="/icons/mono/findnext.svg" alt="next" /></div><!--
--><div id="replaceb" title="replace" ><img src="/icons/mono/replace.svg" alt="replace" /></div><!--
--><div id="filterb" title="filter through pipe" ><img src="/icons/mono/terminal.svg" alt="pipe" /></div><!--
--><div id="readb" title="insert file contents" ><img src="/icons/mono/exitapp.svg" alt="read file" /></div><!--
--></div>  
                    </div>
                </div>

                <!-- spacer div -->
                <div id="spacer"></div>

                <!-- inner and outer wrappers for the editor, plus one to hold it -->
                <div id="ke-outerwrapper">
                    <div id="ke-innerwrapper">
                        <div id="keDiv">
                        </div>
                    </div>
                </div>

            </div> <!-- end of maincontents -->

        </div> <!-- end of allcontents -->
        <!-- end of document -->
    </body>
</html>