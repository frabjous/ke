<?php 

session_start();
require $_SERVER["DOCUMENT_ROOT"] . '/kcklib/pipe.php';

function rage_quit($errcode) {
    echo '☹☹ ERROR: ' . $errcode . ' ☹☹';
    exit(1);
}

require_once 'default_authentication.php';

if (!((isset($_POST["filecontents"])) and (isset($_POST["pipe"])))) {
   rage_quit("File contents or pipe command not provided.");
}

$sent_contents = $_POST["filecontents"];
$sent_contents = preg_replace('~\R~u', PHP_EOL, $sent_contents);

$pipe = $_POST["pipe"];

if (!$ke_poweruser) {
    if (strpos($pipe, 'sudo') !== false) {
        rage_quit("Non-powerusers may not use sudo in a pipe.");
    }
    if (strpos($pipe, '/') !== false) {
        rage_quit("Non-powerusers may not use slashes in pipes.");
    }
    if (strpos($pipe, '..') !== false) {
        rage_quit("Non-powerusers may not use double dots in pipes.");
    }

}

$results = pipe_to_command($pipe, $sent_contents);

if ($results->returnvalue != 0) {
   rage_quit($results->returnvalue . ' - ' . $results->stderr);
}

echo $results->stdout;

exit(0);