<?php

session_start();

$ret_val = new stdClass();

$ret_val->responseType = "process-save"; 

function rage_quit($errcode) {
    global $ret_val;
    $ret_val->errmsg = $errcode;
    echo(json_encode($ret_val, JSON_PRETTY_PRINT));
    exit($errcode);
}

require_once 'default_authentication.php';
require_once 'get_folder_locations.php';

if (!(isset($_POST["filecontents"]))) {
    rage_quit("file contents not provided");
}
if (!(isset($_POST["filename"]))) {
    rage_quit("filename not provided");
}
if (!(isset($_POST["generation"]))) {
    rage_quit("generation number not provided");
}

// check if can save in this folder
if (!$ke_poweruser) {
    $rp = realpath($_POST["filename"]);
    $ok_to_save = false;
    foreach ($_SESSION["_ke_allowed_folders"] as $folder) {
        if (substr($rp, 0, strlen($folder)) == $folder) {
            $ok_to_save = true;
            break;
        }
    }

    if (!$ok_to_save) {
        rage_quit("KE user does not have the permissions to save a file in this folder.");
    }
}

$sent_contents = $_POST["filecontents"];
$sent_contents = preg_replace('~\R~u', PHP_EOL, $sent_contents);

$olddir = getcwd();
// change working dir to saved file
if (isset($_SESSION["_ke_workingdir"])) {
    chdir($_SESSION["_ke_workingdir"]);
}

if (file_exists($_POST["filename"])) {
    $filename_to_save = realpath($_POST["filename"]);
} elseif (substr($_POST["filename"], 0, 1) == "/") {
    $filename_to_save = $_POST["filename"];
} else {
    $filename_to_save = getcwd() . '/' . $_POST["filename"];
}

$_SESSION["_ke_workingdir"] = dirname($filename_to_save);
chdir($_SESSION["_ke_workingdir"]);

// archive file
if (!(isset($_SESSION["savedfiles"]))) {
    $_SESSION["savedfiles"] = array();
}

if ((file_exists($filename_to_save)) and (!(in_array($filename_to_save, $_SESSION["savedfiles"])))) {
    $archive_name = $ke_folder_locations->archive . '/' . date("Y-m-d-H-i-s") . mb_ereg_replace("/","⊃",$filename_to_save);
    if (!(copy($filename_to_save, $archive_name))) {
        rage_quit("Could not create archive file.");
    }
    array_push($_SESSION["savedfiles"], $filename_to_save);
}

// actual save command
$saveresult = file_put_contents($filename_to_save, $sent_contents);
if (!($saveresult)) {
    rage_quit("Save failed.");
}


// include save success in return vale
$ret_val->saveSuccess = true;
$ret_val->savedFileName = $filename_to_save;
$ret_val->generation = $_POST["generation"];

// post-save processing
$process_method = $_POST["processMethod"] ?? '';
$processor_file_name = $olddir . '/ftplugin/' . $process_method . '/process.php';
if (($process_method != '') and (file_exists($processor_file_name) ) ) {
    include $processor_file_name;
} 

// print return value
echo(json_encode($ret_val, JSON_PRETTY_PRINT));
exit(0);
