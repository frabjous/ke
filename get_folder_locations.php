<?php

// archive, backup, and autosave folders are by
// default in $HOME/tmp/.kearchive, etc.
// but can be set in another location
// by creating a json file, folder-locations.json
// with the following format:
/*

{
    "archive": "/path/to/archive_files",
    "backup": "/path/to/backup_files",
    "autosave": "/path/to/autosave_files"
}

*/

if (file_exists('folder-locations.json')) {
    $ke_folder_locations = json_decode(file_get_contents('folder-locations.json'));
} else {
    $ke_folder_locations = new StdClass();
    $home_folder = getenv("HOME");
    $ke_folder_locations->archive = $home_folder . '/tmp/.kearchive';
    $ke_folder_locations->backup = $home_folder . '/tmp/.kebackup';
    $ke_folder_locations->autosave = $home_folder . '/tmp/.keautosave';
}

foreach ($ke_folder_locations as $kind => $folder) {
    if (!is_dir($folder)) {
        mkdir($folder,0700,true);
    }
}
