<?php

function mode_array($mt, $ext, $mfa, $usw = false) {
    return array(
        "mimetype" => $mt,
        "extension" => $ext,
        "modefiles" => $mfa,
        "usewrap" => $usw
    );
}

function get_mode_for_file($fn) {
    $mt = '';
    if (file_exists($fn)) {
        $mt = mime_content_type($fn);
    }
    $pp = pathinfo($fn);
    if (isset($pp['extension'])) {
        $ext = mb_strtolower($pp['extension']);
    } else {
        $ext = '';
    }

    // markdown
    if (($ext == 'md') or ($ext == 'markdown') or ($ext == "mdown")) {
        return mode_array('markdown', 'markdown', array('xml', 'markdown'), true);
    }

    // javascript
    if (($mt == 'text/javascript') or ($mt == 'application/javascript') or ($ext == 'js')) {
        return mode_array('text/javascript', 'js', array('javascript'));
    }

    // html
    if (($mt == 'text/html') or ($ext == 'html') or ($ext == 'htm') or ($ext == 'xhtml')) {
        return mode_array('text/html', 'html', array('xml', 'javascript', 'css', 'htmlmixed'), true);
    }

    // php    
    if (($mt == 'text/x-php') or ($mt == 'application/x-httpd-php') or ($ext == 'php')) {
        return mode_array('application/x-httpd-php', 'php', array('xml', 'javascript', 'css', 'htmlmixed', 'clike', 'php'), true);
    }

    // css
    if (($mt == 'text/css') or ($ext == 'css')) {
        return mode_array('text/css', 'css', array('css'));
    }

    if (($ext == 'lua') or (($mt == 'text/x-lua') or ($mt == 'application/x-lua'))) {
        return mode_array('text/x-lua', 'lua', array('lua'));
    }

    // rust
    if (($ext == 'rs') or ($mt == 'text/x-rustsrc')) {
        return mode_array('text/x-rustsrc', 'rust', array('rust'));
    }

    // toml 
    if (($mt == 'text/x-toml') or ($ext == 'toml' )) {
        return mode_array('text/x-toml', 'toml', array('toml'));
    }

    // latex
    if (($mt == 'text/x-tex') or ($mt == 'text/x-stex') or ($ext == 'tex') or ($ext == 'sty') or ($ext == 'cls') or ($ext == 'ltx') or ($ext == 'bbl')) {
        return mode_array('text/x-stex', 'tex', array('stex'), true);
    }

    // bash, sh
    if (($mt == "text/x-sh") or ($ext == "sh") or ($ext == "bash") or ($ext == "bashrc") or (basename($fn) == "bashrc") ) {
        return mode_array('text/x-sh', 'sh', array('shell'));
    }

    // json
    if (($mt == 'application/json') or ($mt == 'text/json') or ($ext == 'json')) {
        return mode_array('application/json', 'json', array('javascript'), true);	
    }

    // xml
    if (($mt == 'text/xml') or ($mt == 'application/xml') or ($ext == 'xml') or ($ext == 'svg')) {
        return mode_array('text/xml', 'xml', array('xml'));
    }

    //http headers
    if (($mt == 'message/http') or ($ext == 'http')) {
        return mode_array('message/http', 'http', array('http'));
    }

    // perl
    if (($mt == 'text/x-perl') or ($ext == 'pl')) {
        return mode_array('text/x-perl', 'pl', array('perl')); 
    }

    // python
    if (($mt == 'text/x-python') or ($ext == 'py')) {
        return mode_array('text/x-python', 'py', array('python'));
    }

    // diff
    if (($mt == 'text/x-diff') or ($ext == 'diff') or ($ext == 'patch')) {
        return mode_array('text/x-diff', 'diff', array('diff'));
    }

    // mail
    if (($mt == 'application/mbox') or ($ext == 'mbox') or ($ext == 'mail') or ($ext == 'email')) {
        return mode_array('application/mbox', 'mbox', array('mbox'), true);	
    }

    // plain text
    return mode_array('text/plain', 'txt', array(), true);

}