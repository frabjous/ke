<?php
session_start();

function rage_quit($errmsg) {
    echo "Error:  " . $errmsg;
    exit(1);
}

require_once 'default_authentication.php';
require_once 'get_folder_locations.php';

if (!(isset($_POST["filename"]))) {
    rage_quit("filename not set");
}

$fn = $_POST["filename"];

if (file_exists($fn)) {

    if (!$ke_poweruser) {
        $rp = realpath($fn);
        $ok_to_read = false;
        foreach ($_SESSION["_ke_allowed_folders"] as $folder) {
            if (substr($rp, 0, strlen($folder)) == $folder) {
                $ok_to_read = true;
                break;
            }
        }

        if (!$ok_to_read) {
            rage_quit("KE user does not have the permissions to read that file.");
        }
    }

    $bu = mb_ereg_replace("/","⊃","$fn");
    if (!(copy($fn, $ke_folder_locations->backup . "/" . date("Y-m-d-H-i-s",filemtime($fn)) . $bu))) {
        rage_quit("Could not create backup file.");
    }
    readfile($fn);
}
exit(0);
