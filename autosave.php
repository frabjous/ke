<?php

session_start();

$ret_val = new stdClass();

$ret_val->responseType = "auto-save"; 

function rage_quit($errmsg) {
    global $ret_val;
    $ret_val->errmsg = $errmsg;
    echo(json_encode($ret_val, JSON_PRETTY_PRINT));
    exit($errcode);
}

require_once 'default_authentication.php';
require_once 'get_folder_locations.php';

if (!(isset($_POST["filecontents"]))) {
    rage_quit("filecontents not included in post");
}
if (!(isset($_POST["filename"]))) {
    rage_quit("filename not included in post");
}
if (!(isset($_POST["generation"]))) {
    rage_quit("generation number not included in post");
}

$sent_contents = $_POST["filecontents"];
$sent_contents = preg_replace('~\R~u', PHP_EOL, $sent_contents);

$filename_to_save = $_POST["filename"];

if ($filename_to_save == '') {
    $filename_to_save = 'temp';
}

$autosave_name = $ke_folder_locations->autosave . '/' . date("Y-m-d-H-i-s") . mb_ereg_replace("/","⊃",$filename_to_save);

// actual save command
$saveresult = file_put_contents($autosave_name, $sent_contents);

if (!($saveresult)) {
    rage_quit("Could not autosave.");
}

$ret_val->saveSuccess = true;
$ret_val->savedFileName = $filename_to_save;
$ret_val->generation = $_POST["generation"];

echo(json_encode($ret_val, JSON_PRETTY_PRINT));
exit(0);
