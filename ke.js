
function showResults() {
    KE.results = window.open(KE.resultsLoc,'results','menubar=no,status=no,scrollbars=yes,width=500,height=500');
    setTimeout(
        function() { KE.results.location.href = KE.resultsLoc; return false; }, 100
    );
    KE.prevIsOpen = true;
    fixMCHeight();
    return false;
}

function hideResults() {
    KE.results.close();
    KE.prevIsOpen = false;
    fixMCHeight();
}

function topPromptTemplate(labelText, hintText = '', inpValue = '') {
    var d=document.createElement("div");
    d.label=creAdd("label", d);
    d.label.innerHTML = labelText;
    d.input = creAdd("input", d, ['CodeMirror-search-field'], "selsrsearch");
    d.input.value = inpValue;
    d.input.autofocus = true;
    d.input.style.width = '10em';
    d.input.id = 'prompt' + Math.random().toString();
    d.label.htmlFor = d.input.id;
    d.hint=creAdd("span", d, ['CodeMirror-search-hint']);
    d.hint.style.color = '#888';
    d.hint.innerHTML= hintText;
    return d;
}

function makeButtonInactive(b) {
    b.classList.remove("normal","processing","errorbutton");
    b.classList.add("inactive");
}

function makeButtonNormal(b) {
    b.classList.remove("inactive","processing","errorbutton");
    b.classList.add("normal");
}

function makeButtonProcessing(b) {
    b.classList.remove("normal","inactive","errorbutton");
    b.classList.add("processing");
}

function makeButtonError(b) {
    b.classList.remove("normal","processing","inactive");
    b.classList.add("errorbutton");
}

function respondToMessage(text) {
    resp = JSON.parse(text);
    if (!(resp.hasOwnProperty("responseType"))) {
        kckErrAlert("Unknown response from server.");
        return;
    }
    if (resp.responseType == "process-save") {
        if (((resp.hasOwnProperty("saveSuccess")) && (resp.saveSuccess))) {
            if ((resp.hasOwnProperty("generation")) && KE.myCM.isClean(resp.generation)) {
                KE.markClean();
                KE.currentGeneration = KE.myCM.changeGeneration();
                if ((resp.hasOwnProperty("savedFileName")) && (resp.savedFileName != KE.filename)) {
                    KE.potentialFileName = resp.savedFileName;
                    kckYesNoBox("Reload as " + resp.savedFileName + "?", function() {
                        KE.open(KE.potentialFileName, true);
                    });
                }
            } else {
                makeButtonNormal(KE.saveButton);
            }
        } else {
            var errmsg = "Save was not successful!";
            if (resp.hasOwnProperty("errmsg")) {
                errmsg += ' Server reports: ' + resp.errmsg;
            }
            kckErrAlert(errmsg);
            makeButtonError(KE.saveButton);
        }
        if (resp.hasOwnProperty("processSuccess")) {
            KE.postSaveHandlers[KE.handlerName](resp);
        }           
        return;
    }
    if (resp.responseType == "auto-save") {
        if (((resp.hasOwnProperty("saveSuccess")) && (resp.saveSuccess))) {
            KE.startAutoSaveTimer();
            KE.myCM.openNotification("Autosaved", { duration: 800 });
        } else {
            if (resp.hasOwnProperty("errmsg")) {
                kckErrAlert("Autosave failed. Server responds: " + resp.errmsg);
            } else {
                kckErrAlert("Autosave failed.");
            }
        }
        return;
    }
    kckErrAlert("Server sent response of unknown type: " + text);
}

function isOpenAction(action) {
    if ((action == "open") ||
        (action == "o") ||
        (action == "edit") ||
        (action == "e")) {
        return true;
    }
    return false;
}

function isSaveAction(action) {
    if ((action == "write") ||
        (action == "w") ||
        (action == "saveas") ||
        (action == "save") ||
        (action == "s")) {
        return true;
    }
    return false;
}

function isWrapAction(action) {
    if ((action == "wrap") ||
        (action == "togglewrap") ||
        (action == "tw") ||
        (action == "wt")) {
        return true;
    }
    return false;
}


function kevEditor(parNode = document.body, theme = 'cobalt', mode = 'text/plain',  fn = '', usewrap = false) {
    this.filename = fn;
    this.isSaved = true;
    this.origDocTitle = document.title;

    this.markClean = function() {
        document.title = this.origDocTitle;
        makeButtonInactive(this.saveButton);
        this.currentGeneration = this.myCM.changeGeneration();
    }

    this.markDirty = function() {
        document.title = "+ " + KE.origDocTitle;
        if (this.saveButton.classList.contains("inactive")) {
            makeButtonNormal(this.saveButton);
        }
    }

    this.myCM = CodeMirror(parNode, {
        mode : mode, 
        theme : theme,
        lineWrapping: usewrap,
        lineNumbers: true,
        matchBrackets: true,
        autofocus: true,
        indentUnit: 4,
        extraKeys: {
            "Alt-S": function(cm) {
                cm.save();
            },
            "Ctrl-S": function(cm) {
                cm.save();
            },
            "Tab": "indentMore",
            "F3": function(cm) {
                CodeMirror.commands.findNext(cm);
            },
            "Ctrl-R" : function(cm) {
                KE.replaceDialog();
            },
            "Ctrl-D" : function(cm) {
                KE.duplicateLine();
            },
            "Ctrl-K" : function(cm) {
                KE.killRestOfLine();
            },
            "Ctrl-;" : function(cm) {
                KE.openCommandLine();
            },          
            "Ctrl-Space" : function(cm) {
                KE.openCommandLine();
            },
            "Ctrl-," : function(cm) {
                KE.toggleWrap();
            },
            "Ctrl-5" : function(cm) {
                KE.goToMatchingBracket();
            },
            "Shift-Ctrl-\\" : function(cm) {
                KE.pipeDialog();
            },
            "Ctrl-Up" : function(cm) {
                cm.execCommand("goLineUp");
                cm.execCommand("goLineEnd");
                cm.execCommand("newlineAndIndent");
            },
            "Ctrl-Down" : function(cm) {
                cm.execCommand("goLineEnd");
                cm.execCommand("newlineAndIndent");
            },
            "Alt-Down" : function(cm) {
                KE.moveLineDown();
            },
            "Alt-Up" : function(cm) {
                KE.moveLineUp();
            }

        }
    });
    this.currentGeneration = this.myCM.changeGeneration();

    this.goToMatchingBracket = function() {
        var m = KE.myCM.findMatchingBracket(KE.myCM.getCursor(), false);
        if (m.match) {
            KE.myCM.setCursor(m.to);
        }
    }

    this.replaceDialog = function() {
        KE.selSearchReplaceDialog(this.myCM.somethingSelected());
    }

    this.postSaveHandlers = {};
    this.handlerName = '';

    this.initialOpen = function(fn) {
        var fD = new FormData();
        fD.append('filename', fn);
        AJAXPostRequest('read_file.php', fD, function(text) {
            KE.myCM.setValue(text);
            KE.markClean();
            KE.myCM.clearHistory();
            setTimeout(function() {
                KE.myCM.refresh();
            }, 1);
        });
    }

    // SAVE FUNCTION
    this.save = function(fn = KE.filename, processMethod = '') {
        if ((fn == '') || (/\/$/.test(fn)) || (!(/^\//.test(fn)))) {
            KE.possNewFilenamePrompt = topPromptTemplate('Save as: ', '');
            KE.possNewFilenamePrompt.input.value = KE.workingDir + '/';
            if ((fn != '') && (!(/\/$/.test(fn)))) {
                KE.possNewFilenamePrompt.input.value += fn;
            }
            KE.possNewFilenamePrompt.input.style.width='30em';
            var ch = KE.myCM.openDialog(KE.possNewFilenamePrompt, function() {
                KE.save(KE.possNewFilenamePrompt.input.value);
            });
            return;
        }
        makeButtonProcessing(KE.saveButton);
        var gen=KE.myCM.changeGeneration();
        var cmText = KE.myCM.getValue();
        var fD = new FormData();
        fD.append('filecontents', cmText);
        fD.append('filename', fn);
        fD.append('generation', gen);
        var cp = KE.myCM.getCursor();
        fD.append('cursorline', cp.line);
        fD.append('cursorch', cp.ch);
        if (processMethod != '') {
            fD.append('processMethod', processMethod);
        }
        AJAXPostRequest('process_save.php', fD, function(rT) {
            respondToMessage(rT); 
        });
    }

    this.autoSave = function(fn = KE.filename) {
        if (!(KE.myCM.isClean(KE.autoSaveGeneration))) {
            var gen = KE.autoSaveGeneration;
            var cmText = KE.myCM.getValue();
            var fD = new FormData();
            fD.append('filecontents', cmText);
            fD.append('filename', fn);
            fD.append('generation', gen);
            AJAXPostRequest('autosave.php', fD, function(rT) {
                respondToMessage(rT); 
            });
        } else {
            KE.startAutoSaveTimer();
        }
    }

    // open function
    this.open = function(f, sametab = false) {
        var url = [location.protocol, '//', location.host, location.pathname].join('');
        url += '?file=' + encodeURIComponent(f);
        if (sametab) {
            window.open(url, '_self');
        } else {
            window.open(url, '_blank');
        }
    }
    this.myCM.save = this.save;

    this.startAutoSaveTimer = function() {
        KE.autoSaveGeneration = KE.myCM.changeGeneration();
        KE.autoSaveTimeOut = setTimeout( function(){
            KE.autoSave();
        }, 600000);
    }

    this.changeFont = function() {
        AJAXGetRequest('/kcklib/getfonts.php', '', function(text) {
            var fontlist = JSON.parse(text);
            kckGenericListSelection(fontlist, 'Choose a font family:', function(o) {
                document.getElementById("keDiv").getElementsByClassName("CodeMirror")[0].style.fontFamily = o;
                KE.myCM.refresh();
            });
        });
    }

    this.toggleWrap = function() {
        if (this.myCM.getOption("lineWrapping")) {
            this.myCM.setOption("lineWrapping",false);
            makeButtonInactive(KE.wrapButton);
        } else {
            this.myCM.setOption("lineWrapping",true);
            makeButtonNormal(KE.wrapButton);
        }
    }

    this.selSearchReplaceDialog = function (somethingSelected) {
        if (somethingSelected) {
            KE.selSR = topPromptTemplate('(In selection) replace: ', '(Use /re/ syntax for regexp search)');
        } else {
            KE.selSR = topPromptTemplate('Replace all: ', '(Use /re/ syntax for regexp search)');
        }
        var cf = KE.myCM.openDialog(KE.selSR, function () { KE.selSearchReplaceNextDialog(); });
    }

    this.selSearchReplaceNextDialog = function () {
        if (KE.myCM.somethingSelected()) {
            KE.selSRWith = topPromptTemplate('(In selection) with: ');
        } else {
            KE.selSRWith = topPromptTemplate('With: ');
        }
        var cf = KE.myCM.openDialog(KE.selSRWith, function () { KE.selSearchReplace(); });
    }

    this.selSearchReplace = function () {
        var sq=KE.selSR.input.value;
        var repl=KE.selSRWith.input.value;
        var useregexp = false;
        var spl = sq.substr(1).split("/");
        if ((/^\//.test(sq)) && (spl.length > 1)) {
            useregexp = true;
            options = spl[(spl.length - 1)];
            var regexstr = spl.slice(0,-1).join('\/');
            var regexq = new RegExp(regexstr, options + 'g');           
        }
        if (KE.myCM.somethingSelected()) { 
            var selText = KE.myCM.getSelection();
        } else {
            var selText = KE.myCM.getValue();
        }
        if (useregexp) {
            var q=regexq;
            var newText = selText.replace(q, repl);
        } else {
            var q=sq;
            var newText = selText.split(q).join(repl);
        }
        if (KE.myCM.somethingSelected()) { 
            KE.myCM.replaceSelection(newText, "around");
        } else {
            var cursPos = KE.myCM.getCursor();
            KE.myCM.setValue(newText);
            KE.myCM.setCursor(cursPos);
            KE.myCM.refresh();
        }
    }

    this.pipeDialog = function() {
        KE.pipeDlg = topPromptTemplate("Pipe: ",'',KE.prevPipe);
        if (KE.prevPipe != '') {
            KE.pipeDlg.input.select();
        }
        var cf=KE.myCM.openDialog(KE.pipeDlg, function() {
            KE.prevPipe = KE.pipeDlg.input.value;
            KE.pipeThrough(KE.pipeDlg.input.value);
        });
    }

    this.readFileDialog = function() {
        KE.readFileDlg = topPromptTemplate("Read: ",'');
        KE.readFileDlg.input.value=KE.workingDir + "/";
        var cf=KE.myCM.openDialog(KE.readFileDlg, function() {
            KE.pipeThrough('cat ' + KE.readFileDlg.input.value);
        });
    }

    this.pipeThrough = function(pipe) {
        fD = new FormData();
        fD.append("pipe", pipe);
        if (KE.myCM.somethingSelected()) {
            fD.append("filecontents", KE.myCM.getSelection());
        } else {
            fD.append("filecontents", KE.myCM.getValue());
        }
        AJAXPostRequest('filter.php', fD, function(text) {
            text = text.trim();
            if (text.substring(0,2) == '☹☹') {
                kckErrAlert(text);
                return;
            }
            if (KE.myCM.somethingSelected()) {
                KE.myCM.replaceSelection(text, "around");
            } else {
                KE.myCM.setValue(text);
                setTimeout(function() {
                    KE.myCM.refresh();
                }, 1);
            }
        });
    }

    this.duplicateLine = function() {
        var lineno = KE.myCM.getCursor().line;
        var linetext = KE.myCM.getLine(lineno);
        KE.myCM.replaceRange(linetext + "\n", {ch:0, line:lineno});
    }

    this.moveLineDown = function() {
        var c = KE.myCM.getCursor();
        var lineno = c.line;
        var spos = c.ch;
        var lastline = KE.myCM.lastLine();
        if (lineno >= lastline) {
            return;
        }
        var linetext = KE.myCM.getLine(lineno);
        var nextline = KE.myCM.getLine(lineno+1);
        var nll = nextline.length;
        KE.myCM.replaceRange(
            nextline + "\n" + linetext,
            {ch:0, line:lineno},
            {ch:nll, line:(lineno+1)}   
        );
        KE.myCM.setCursor( { ch:spos, line:(lineno+1)} );
    }

    this.moveLineUp = function() {
        var c = KE.myCM.getCursor();
        var lineno = c.line;
        var spos = c.ch;
        if (lineno <= 0) {
            return;
        }
        var linetext = KE.myCM.getLine(lineno);
        var prevline = KE.myCM.getLine(lineno-1);
        var ll = linetext.length;
        KE.myCM.replaceRange(
            linetext + "\n" + prevline,
            {ch:0, line:(lineno-1)},
            {ch:ll, line:lineno}
        );
        KE.myCM.setCursor( { ch:spos, line:(lineno-1)} );
    }

    this.killRestOfLine = function() {
        var c = KE.myCM.getCursor();
        var lineno = c.line;
        var spos = c.ch;
        var ll = KE.myCM.getLine(lineno).length;
        KE.myCM.replaceRange(
            '',
            {ch:spos, line:lineno},
            {ch:ll, line:lineno}
        );
    }

    this.onchangeExtras = function() {};
    this.autoProcess = false;
    this.autoProcessTO = 0;
    this.autoProcessDelay = 1000;
    this.prevIsOpen = false;
    this.resultsLoc = '';
    this.processOption = '';

    // when editor changes
    this.myCM.on("change", function(i, e) {
        KE.markDirty();
        KE.onchangeExtras();
    });

    this.prevPipe = '';
}
