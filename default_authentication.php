<?php

// Authentication for KE involves two session variables;
// if $_SESSION["_ke_poweruser"] is set, and set to true, the user can edit any file
// the webserver can access.
// otherwise, $_SESSION["_ke_allowed_folders"] should be an array of folder names
// the files in which can be accessed or edited

// To customize the rules of access, create a file 'authenticate.php' either in the
// KE folder or webserver's document root, which should either set these variables or
// redirect to another page/pages that will result in this happening

// If neither of these exist, the default
// authentication protocal will be used.

if (file_exists('authenticate.php')) {
    require 'authenticate.php' ;
} elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . '/authenticate.php')) {
    require $_SERVER['DOCUMENT_ROOT'] . '/authenticate.php';
} else {
    // the default authentication protocol grants poweruser access when
    // accessed through localhost, and no access otherwise

    if ($_SERVER["HTTP_HOST"] == "localhost") {
        $_SESSION["_ke_poweruser"] = true;
    }

}

$ke_poweruser = ((isset($_SESSION["_ke_poweruser"])) && ($_SESSION["_ke_poweruser"]));
$ke_reguser = ((isset($_SESSION["_ke_allowed_folders"])) && (count($_SESSION["_ke_allowed_folders"]) > 0) );

$ke_authenticated = (($ke_poweruser) || ($ke_reguser));

// if not authenticated in either way, and one is using the main KE page, it'll redirect
// to a page 'login.php' in the document root if one exists, and set a session variable _ke_loginredirect,
// for the full URL so it can be redirected back to.

// otherwise an authentication error will be delivered and the script will end, using the
// function 'rage_quit' if it exists

if (!$ke_authenticated) {
    if ( substr($_SERVER["PHP_SELF"], -9) == 'index.php') {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/login.php')) {
            $_SESSION["_ke_loginredirect"] = full_url();
            header('Location: ' . full_host() . '/login.php');
            exit(0);
        }
    }
    if (function_exists('rage_quit')) {
        rage_quit('Authentication error.');
    } else {
        echo 'Authentication error.';
        exit(1);
    }
}
